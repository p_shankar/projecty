<?php
$criteria = stripslashes($_POST['criteria']);
$criteria = json_decode($criteria, true);

//$feed = $criteria['feed'];
$feed = 'nearby';
$address = $criteria['address'];
$lat = $criteria['lat'];
$lng = $criteria['lng'];

//API call
$apiCriteria['feed'] = $feed;
$apiCriteria['address'] = urlencode($address);
$apiCriteria['lat'] = $lat;
$apiCriteria['lng'] = $lng;
$apiCriteria['distance_unit'] = $GLOBALS['distance_unit'];
$url = getAPICallUrl($apiCriteria);

//Get data from API
$data = getDataFromUrl($url);
$data2 = json_decode($data, true);

$stores = getNearbyDisplay($data2);

$display['display'] = $stores;

$display = json_encode($display);
echo $display;
?>
