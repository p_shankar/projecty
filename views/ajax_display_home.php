<ul data-role="listview" data-theme="d" style="margin-bottom:15px;"  data-inset="true">
	<hr class="ornamental"/>	<li data-role="list-divider">Check in</li>
<li><a href="#nearbyPage" id="displayNearbyListBtn">Find what's nearby</a></li><hr class="ornamental"/><br />
	<li data-role="list-divider">Redeem points</li>
	<li><a href="#listPage" id="displayStoresListBtn">List of stores and offers nearby</a></li>
	<li><a href="#mapPage" id="displayMapBtn">Map of stores and offers nearby</a></li>
	<li><a href="#searchPage">Search stores and offers nearby</a></li>
</ul>
<br>

<hr>
<small>Powered by <a href="http://161mobility.com">161Mobility</a></small>
