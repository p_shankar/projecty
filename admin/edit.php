<?php
include('../api/include/webzone.php');

$id = $_GET['id'];

$t1 = new Template_class_admin();
$t1->setPageName('Edit a store');
$t1->setMetaTags(array('title'=>'', 'description'=>''));
$t1->displayHeader();

$c1 = new Store_locator_category();
$list = $c1->selectAll(array('order'=>'id DESC'));
for($i=0; $i<count($list); $i++) {
	$list_tab[$list[$i]['id']] = $list[$i]['name'];
}

$s1 = new Store_locator();
$store = $s1->loadByFields('id',$id);

//form
$criteria['fields'][] = array('name'=>'category_id', 'title'=>'Category:', 'type'=>'select', 'select_values'=>$list_tab, 'value'=>$store[0]['category_id']);
$criteria['fields'][] = array('name'=>'name', 'title'=>'Name:', 'value'=>$store[0]['name']);
$criteria['fields'][] = array('name'=>'address', 'title'=>'Address:', 'value'=>$store[0]['address']);
$criteria['fields'][] = array('name'=>'logo', 'title'=>'Logo url:', 'value'=>$store[0]['logo']);
$criteria['fields'][] = array('name'=>'url', 'title'=>'URL:', 'value'=>$store[0]['url']);
$criteria['fields'][] = array('name'=>'description', 'title'=>'Description:', 'type'=>'textarea', 'rows'=>'5', 'value'=>$store[0]['description']);
$criteria['fields'][] = array('name'=>'tel', 'title'=>'Tel:', 'value'=>$store[0]['tel']);
$criteria['fields'][] = array('name'=>'email', 'title'=>'Email:', 'value'=>$store[0]['email']);
$criteria['submit'] = array('name'=>'edit', 'value'=>'Edit store');

if($_POST[$criteria['submit']['name']]) {
	
	$values = get_post_values($criteria['fields'], $_POST);
	
	$g1 = new Gmap_class();
	$geocode = $g1->geoCode(urlencode($values['address']));
	
	$s1 = new Store_locator();
	
	$criteria = array('category_id'=>$values['category_id'], 'name'=>$values['name'], 'address'=>$values['address'], 'logo'=>$values['logo'], 'url'=>$values['url'],
	'description'=>$values['description'],'tel'=>$values['tel'],'email'=>$values['email'],'lat'=>$geocode['lat'],'lng'=>$geocode['lng']);
	$s1->updateByFields($criteria, $id);
	
	echo '<script>';
	echo 'window.location="./list.php";';
	echo '</script>';
	
	//echo '<p>You store information has been updated.</p>';
	//echo '<a href="./list.php">Stores list</a>';
}

else {
	echo '<div style="width:600px;">';
	display_forms($criteria);
	echo '</div>';
}

$t1->displayFooter();
?>