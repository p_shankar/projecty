/*
Updates to the new version
*/

ALTER TABLE  `store_locator` ADD  `category_id` INT NOT NULL AFTER  `post_id`

CREATE TABLE IF NOT EXISTS `store_locator_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(120) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
