CREATE TABLE IF NOT EXISTS `store_locator` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `post_id` bigint(20) NOT NULL,
  `category_id` int(11) NOT NULL,
  `name` varchar(160) NOT NULL,
  `logo` varchar(160) NOT NULL,
  `address` varchar(160) NOT NULL,
  `lat` varchar(20) NOT NULL,
  `lng` varchar(20) NOT NULL,
  `url` varchar(160) NOT NULL,
  `description` text NOT NULL,
  `tel` varchar(30) NOT NULL,
  `email` varchar(60) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `store_locator_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(120) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

/*
Optional data to populate the database with testing data
*/

INSERT INTO `store_locator` (`id`, `user_id`, `post_id`, `category_id`, `name`, `logo`, `address`, `lat`, `lng`, `url`, `description`, `tel`, `email`, `created`) VALUES
(10, 0, 0, 1, 'Petro Canada Saint Michel', 'http://yougapi.com/include/images/products/petro-canada.png', '7515-7517 Boulevard Saint Michel', '45.560984', '-73.602396', 'http://www.petro-canada.ca/', '', '', '', '2011-05-23 14:58:45'),
(2, 0, 0, 1, 'Petro Canada Salaberry', 'http://yougapi.com/include/images/products/petro-canada.png', '2525 Rue de Salaberry, Montreal, Canada', '45.5384103', '-73.6988016', 'http://www.petro-canada.ca/', '', '', '', '2011-05-23 14:59:40'),
(8, 0, 0, 3, 'Esso Mont-Royal', 'http://yougapi.com/include/images/products/esso.png', '957 Avenue du Mont-Royal Est, Montreal, Canada', '45.5272796', '-73.580086', 'http://www.esso.com/', '', '', '', '2011-05-23 15:00:46'),
(4, 0, 0, 3, 'Esso Rosemont', 'http://yougapi.com/include/images/products/esso.png', '1125 Boulevard Rosemont, Montreal, Canada', '45.5349502', '-73.59494', 'http://www.esso.com/', '', '', '', '2011-05-23 15:01:38'),
(5, 0, 0, 4, 'KANDA SUSHI BAR - BISHOP', 'http://yougapi.com/include/images/products/kanda.png', '2045 Bishop, Montreal Quebec H3G 2E8, Canada', '45.4976696', '-73.5783756', 'http://www.kanda-sushi.com/', 'Good sushi restaurant', '(514) 845-8868', '', '2011-05-23 15:13:32'),
(9, 0, 0, 4, 'KANDA SUSHI BAR - SAINTE-CATHERINE', 'http://yougapi.com/include/images/products/kanda.png', '537 Sainte-Catherine West, Montreal Quebec H3B 1B2, Canada', '45.5043498', '-73.5687027', 'http://www.kanda-sushi.com/', '', '(514) 288-3868', '', '2011-05-23 15:14:31'),
(7, 0, 0, 4, 'McDonald''s Atwater', 'http://yougapi.com/include/images/products/mcdonalds.png', '1500, AV. ATWATER  MONTRÉAL, QC H3Z 1X5', '45.489216', '-73.584967', 'http://www.mcdonalds.ca', '', '', '', '2011-05-23 15:18:12'),
(11, 0, 0, 2, 'Ultramar', 'http://yougapi.com/include/images/products/ultramar.png', '1528 Boulevard Saint Jean Baptiste, Montreal', '45.6422979', '-73.5050461', 'http://www.ultramar.ca/', '', '', '', '2011-05-23 15:43:56');

INSERT INTO `store_locator_category` (`id`, `name`) VALUES
(1, 'Petro Canada'),
(2, 'Ultramar'),
(3, 'Esso'),
(4, 'Restaurants');

