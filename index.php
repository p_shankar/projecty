<?php
include('include/webzone.php');

$js_on_ready = "detectLocation(); displayHome();";

include('include/presentation/header.php');
?>

<div data-role="page" data-theme="b" id="homePage">
	
	<?php
	include('include/presentation/header_mobile.php');
	?>
	
	<div data-role="content" >
		  	<br />

		<div id="homeContent"></div>
		
		<?php
		/*
		$url = 'http:///api/?key=&feed=stores&page_number=1&nb_display=5&address=Montreal%20Canada&lat=&lng=&distance_unit=km';
		$data = getDataFromUrl($url);
		$data = json_decode($data, true);
		print_r($data);
		echo '<br><br>';
		echo $data['address'];
		*/
		?>
		
	</div>
	
</div>

<div data-role="page" data-theme="b" id="listPage">
	
	<?php
	include('include/presentation/header_mobile.php');
	?>
	
	<div data-role="content"><br />
		<div id="listContent"></div>
	</div>
	
</div>

<div data-role="page" data-theme="b" id="nearbyPage">
	
	<?php
	include('include/presentation/header_mobile.php');
	?>
	
	<div data-role="content"><br />
		<div id="nearbyContent"></div>
	</div>
	
</div>

<div data-role="page" data-theme="b" id="mapPage" style="width:100%; height:100%;">

	<?php
	include('include/presentation/header_mobile.php');
	?>
	
	<div data-role="content" style="width:100%; height:95%; padding:0px; margin:0px;"><br />
		<div id="map_mobile" style="width:98%; height:95%;"></div>
	</div>
	
</div>

<div data-role="page" data-theme="b" id="mapDetailPage" style="width:100%; height:100%;">

	<?php
	include('include/presentation/header_mobile.php');
	?>
	
	<div data-role="content" style="width:100%; height:95%; padding:0px; margin:0px;"><br />
		<div id="map_mobile_detail" style="width:98%; height:95%;"></div>
	</div>

</div>

<div data-role="page" data-theme="b" id="streetviewPage" style="width:100%; height:100%;">

	<?php
	include('include/presentation/header_mobile.php');
	?>
	
	<div data-role="content" style="width:100%; height:95%; padding:0px; margin:0px;"><br />
		<div id="streetview" style="width:98%; height:95%;"></div>
	</div>

</div>

<div data-role="page" data-theme="b" id="storeDetailPage">

	<?php
	$storeDetailPageFlag=1;
	include('include/presentation/header_mobile.php');
	$storeDetailPageFlag=0;
	?>
	
	<div data-role="content"><br />
		
		<div id="storeDetailContent"></div>
		
	</div>

</div>

<div data-role="page" data-theme="b" id="searchPage">

	<?php
	include('include/presentation/header_mobile.php');
	?>
	
	<div data-role="content"><br />
		
		<div data-role="fieldcontain">
		    <label for="search">Search by address:</label>
		    <input type="search" name="address" id="address" value="" />
			<p>
			<a href="#listPage" id="searchStoresBtn" data-role="button" data-theme="a">Search</a>
			</p>
		</div>
		
	</div>

</div>

<?php
include('include/presentation/footer.php');
?>
