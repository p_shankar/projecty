var loading_img = '<img src="' + App.ajaxurl + '/include/graph/icons/ajax-loader.gif">';
var nb_display = App.nb_display;

function displayHome() {
	$('#homeContent').html(loading_img);
	$.ajax({
	  type: 'POST',
	  url: App.ajaxurl + '/process.php?p=displayHome',
	  success: function(msg){
	  	$('#homeContent').html(msg).page();
	  }
	});
}

$('#displayStoresListBtn').live('click', function(event) {
	event.preventDefault();
	page_number=1;
	displayStoresList({"feed":"stores", "page_number":page_number, "nb_display":nb_display, "lat":App.lat, "lng":App.lng});
});

$('#displayNearbyListBtn').live('click', function(event) {
	event.preventDefault();
	page_number=1;
	displayNearbyList({"feed":"stores", "page_number":page_number, "nb_display":nb_display, "lat":App.lat, "lng":App.lng});
});

$('#displayStoresListNextPreviousBtn').live('click', function(event) {
	event.preventDefault();
	$('#pageNumberReload').html(loading_img);
	var criteria = jQuery('body').data('stores_list_criteria');
	var page_number = $(this).attr('page_number');
	criteria.page_number = page_number;
	displayStoresList(criteria);
});

$('#searchStoresBtn').live('click', function(event) {
	event.preventDefault();
	var address = $('#address').val();
	var page_number=1;
	$('#listContent').html(loading_img);
	displayStoresList({"feed":"stores", "page_number":page_number, "nb_display":nb_display, "address":address});
});

$('.displayStoreDetails').live('click', function(event) {
	event.preventDefault();
	var id = $(this).attr('id');
	displayStoreDetails({"feed":"store", "id":id});
});

function displayStoreDetails(criteria) {
	$('#storeDetailContent').html(loading_img);
	$.ajax({
	  type: 'POST',
	  url: App.ajaxurl + '/process.php?p=displayStoreDetails',
	  dataType: 'json',
	  data: 'criteria=' + JSON.stringify(criteria),
	  success: function(msg){
	  	$('#storeDetailContent').html(msg.display);
	  	$('#storeDetailPage').page('destroy').page();
	  	//save store data
	  	App.store_lat = msg.lat;
	  	App.store_lng = msg.lng;
	  	App.store_name = msg.name;
	  }
	});
}

function displayStoresList(criteria) {
	if($('#listContent').html()=='') $('#listContent').html(loading_img);
	jQuery('body').data('stores_list_criteria', criteria);
	$.ajax({
	  type: 'POST',
	  url: App.ajaxurl + '/process.php?p=displayStoresList',
	  dataType: 'json',
	  data: 'criteria=' + JSON.stringify(criteria),
	  success: function(msg){
	  	$('#listContent').html(msg.display);
	  	$('#listPage').page('destroy').page();
	  }
	});
}

function displayNearbyList(criteria) {
	if($('#nearbyContent').html()=='') $('#nearbyContent').html(loading_img);
	jQuery('body').data('stores_list_criteria', criteria);
	$.ajax({
	  type: 'POST',
	  url: App.ajaxurl + '/process.php?p=displayNearbyList',
	  dataType: 'json',
	  data: 'criteria=' + JSON.stringify(criteria),
	  success: function(msg){
	  	$('#nearbyContent').html(msg.display);
	  	$('#nearbyPage').page('destroy').page();
	  }
	});
}

/*
START Maps display
*/

$('#displayMapBtn').live('click', function(event) {
	event.preventDefault();
	Map_param.zoom=5;
	search_locations({"feed":"stores", "page_number":1, "nb_display":nb_display, "lat":App.lat, "lng":App.lng});
});

$('#displayMapDetailBtn').live('click', function(event) {
	event.preventDefault();
	Map_param.zoom=15;
	init_basic_map('map_mobile_detail', App.store_lat, App.store_lng, App.store_name);
});

$('#displayStreetviewBtn').live('click', function(event) {
	event.preventDefault();
	displayStreetView(App.store_lat, App.store_lng, 'streetview');
});
