<!DOCTYPE html> 
<html> 
<head>
	<title>ProjectY</title>
	<meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
    <link rel="apple-touch-icon" href="<?=$GLOBALS['app_url'];?>include/assets/images/apple-touch-icon.png"/>
	<link rel="stylesheet" href="http://code.jquery.com/mobile/1.0a4/jquery.mobile-1.0a4.min.css" />
	
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">

	<script type='text/javascript'> 
	/* <![CDATA[ */
	var App = {
		ajaxurl: "<?=$GLOBALS['app_url']?>", nb_display: <?=$GLOBALS['nb_display']?>, marker_icon: "<?=$GLOBALS['marker_icon']?>", 
		marker_icon_current: "<?=$GLOBALS['marker_icon_current']?>"
	};
	/* ]]> */
	</script>
	
	<script src="http://code.jquery.com/jquery-1.6.4.min.js"></script>
	<script src="<?=$GLOBALS['app_url'];?>include/js/app.js?<?=time();?>"></script>
	
	<script src="http://maps.google.com/maps/api/js?sensor=true"></script>
	<script src="<?=$GLOBALS['app_url'];?>include/js/map.js?<?=time();?>"></script>
	
	<script src="http://code.jquery.com/mobile/1.0a4.1/jquery.mobile-1.0a4.1.min.js"></script>
   	<link rel="stylesheet" href="<?=$GLOBALS['app_url'];?>include/assets/css/style1.css">   

	<script>
	$(document).ready(function() {
		<?php echo $js_on_ready; ?>
	});
	</script>
	
	<?php
	
	if($GLOBALS['google_analytics']!='') {
	?>
	<script type="text/javascript">
	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', '<?=$GLOBALS['google_analytics']?>']);
	  _gaq.push(['_setDomainName']);
	  _gaq.push(['_trackPageview']);
	 
	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();
	</script>
	<?php
	}
	
	?>
	
</head>

<body>
