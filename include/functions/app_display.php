<?php

function display_store_details($store) {
	/*
	echo '<div data-role="navbar">';
		echo '<ul>';
			echo '<li><a href="#mapDetailPage" id="displayMapDetailBtn" lat="'.$store[0]['lat'].'" lng="'.$store[0]['lng'].'" name="'.$store[0]['name'].'" data-theme="c">Map</a></li>';
			echo '<li><a href="#mapDetailPage" id="displayStreetviewBtn" lat="'.$store[0]['lat'].'" lng="'.$store[0]['lng'].'" data-theme="c">Streetview</a></li>';
		echo '</ul>';
	echo '</div><br>';
	*/
	
	$display .= '    <script>$("#button").click( function()
           {
             alert("You received 10 points!");
           }
        ); </script>';
	
	if($store[0]['name']!='') $display .= '<h2 style="padding-top:0px; margin-top:0px;">'.$store[0]['name'].'</h2>';
	if($store[0]['logo']!='') $display .= '<p><img src="'.$store[0]['logo'].'"></p>';
	if($store[0]['address']!='') $display .= '<p>'.$store[0]['address'].'</p>';
	if($store[0]['description']!='') $display .= '<p>'.$store[0]['description'].'</p>';
	if($store[0]['url']!='') $display .= '<p>Url: <a href="'.$store[0]['url'].'" target="_blank">'.$store[0]['url'].'</a></p>';
	if($store[0]['tel']!='') $display .= '<p>Tel: '.$store[0]['tel'].'</p>';
	if($store[0]['email']!='') $display .= '<p>Email: '.$store[0]['email'].'</p>';
	$display .='<input type="submit" value="Check-in" id="button" />';
	return $display;
}

function getStoresDisplay($data, $page_number, $nb_display) {
	$nb_stores = $data['nb_stores'];
	$list = $data['list'];
	$current_address = $data['address'];
	
	if($current_address=='') $current_address_display = '&nbsp;';
	else $current_address_display=$current_address;
	
	$display .= '<ul data-role="listview" data-theme="d">';
	$display .= '<li data-role="list-divider" data-theme="a">'.$current_address_display.'<span class="ui-li-count">'.$nb_stores.'</span></li>';
	for($i=0; $i<count($list); $i++) {
		$id = $list[$i]['id'];
		$name = $list[$i]['name'];
		$logo = $list[$i]['logo'];
		$address = $list[$i]['address'];
		$distance = $list[$i]['distance'];
		$created = $list[$i]['created'];
		
		$display .= '<li><a href="#storeDetailPage" class="displayStoreDetails" id="'.$id.'">';
		if($logo!='') $display .= '<img src="'.$logo.'">';
		$display .= '<h3>'.$name;
		//if($current_address!='') $display .= ' <font color="red"><small>'.ceil($distance).' km</small></font>';
		$display .= '</h3>';
		if($current_address!='') $display .= '<span class="ui-li-count"><font color="red"><small>'.ceil($distance).' '.$GLOBALS['distance_unit'].'</small></font></span>';
		$display .= '<p>'.$address.'</p>';
		$display .= '</a></li>';
	}
	$display .= '</ul><br>';
	
	$display .= '<div data-role="controlgroup" data-type="horizontal" data-theme="a" style="text-align:right;" >';
		if($page_number>1) $display .= '<a href="javascript:" id="displayStoresListNextPreviousBtn" page_number="'.($page_number-1).'" data-role="button" data-icon="arrow-l" data-theme="d">Previous</a>';
		$display .= '<a href="javascript:" data-role="button" data-theme="d"><span id="pageNumberReload">'.$page_number.'</span></a>';
		if($nb_stores>($page_number*$nb_display)) $display .= '<a href="javascript:" id="displayStoresListNextPreviousBtn" page_number="'.($page_number+1).'" data-role="button" data-icon="arrow-r" data-theme="d">Next</a>';
	$display .= '</div>';
	
	return $display;
}

function getNearbyDisplay($data) {
	$current_address = $data['address'];
	$url = $data['url'];
	
	if($current_address=='') $current_address_display = '&nbsp;';
	else $current_address_display=$current_address;
	
	$display .= '<ul data-role="listview" data-theme="d">';
	$display .= '<li data-role="list-divider" data-theme="a">'.$current_address_display.'</li>';
	

                   // Open the Curl session
                      $session = curl_init($url);

                      // If it's a POST, put the POST data in the body
                      //if ($_POST['path']) {
                          $postvars = '';
                          while ($element = current($_GET)) {
                              $postvars .= urlencode(key($_GET)).'='.urlencode($element).'&';
                              next($_GET);
                          }
                          curl_setopt ($session, CURLOPT_POST, true);
                          curl_setopt ($session, CURLOPT_POSTFIELDS, $postvars);
                      //}

                      // Don't return HTTP headers. Do return the contents of the call
                      curl_setopt($session, CURLOPT_HEADER, false);
                      curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
                      curl_setopt($session, CURLOPT_HTTPAUTH, CURLAUTH_BASIC); 
                      //curl_setopt($session, CURLOPT_USERPWD, $username.':'.urldecode($password));

                      // Make the call
                      $json = curl_exec($session);
                        $obj = json_decode($json);
                        foreach ($obj as $item) {
                        foreach ($item as $name) {
							$place = $name->name;
							if($place!='') $display .= '<li><a href="#" class="displayStoreDetails"><h3>'.$place.'</h3></a></li>';
							}
						}
	$display .= '</ul><br>';
	return $display;
}

?>
